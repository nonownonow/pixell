const webpack = require('webpack');
const WorkBoxPlugin = require('workbox-webpack-plugin');
// const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
// const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = function override(config) {
  config.resolve.fallback = {
    process: require.resolve('process/browser'),
    // zlib: require.resolve('browserify-zlib'),
    stream: require.resolve('stream-browserify'),
    crypto: require.resolve('crypto-browserify'),
    util: require.resolve('util'),
    buffer: require.resolve('buffer'),
    // asset: require.resolve('assert')
  };

  // https://stackoverflow.com/questions/69135310/workaround-for-cache-size-limit-in-create-react-app-pwa-service-worker
  config.plugins.forEach((plugin) => {
    if (plugin instanceof WorkBoxPlugin.InjectManifest) {
      plugin.config.maximumFileSizeToCacheInBytes = 50 * 1024 * 1024;
    }
  });
  config.optimization.minimize = true;
  config.optimization.minimizer = [
    ...config.optimization.minimizer,
    new TerserPlugin({
      extractComments: false,
    }),
  ];
  config.plugins = [
    ...config.plugins,
    // new webpack.ProvidePlugin({
    //   process: 'process/browser.js',
    //   Buffer: ['buffer', 'Buffer'],
    // }),
    // new CssMinimizerPlugin(),
  ];

  return config;
};
