// project import
import Routes from 'routes';
import React, { Suspense } from 'react';

// ==============================|| APP - THEME, ROUTER, LOCAL  ||============================== //

const App = () => {
  return (
    <Suspense fallback="">
      <Routes />
    </Suspense>
  );
};

export default App;
