import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

// third-party
import { Provider as ReduxProvider } from 'react-redux';

// project import
import App from './App';
import './i18n';

import reportWebVitals from './reportWebVitals';
import { StrictMode } from 'react';

// ==============================|| MAIN - REACT DOM RENDER  ||============================== //

const container = document.getElementById('root') as HTMLElement;

const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(
  <StrictMode>
    <BrowserRouter basename="/pixell">
      <App />
    </BrowserRouter>
  </StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
