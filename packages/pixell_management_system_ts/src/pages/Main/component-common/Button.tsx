import { is } from 'date-fns/locale';
import { ComponentPropsWithRef, FC, forwardRef } from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';

export type ButtonProps = (
  | ({ $url: string } & ComponentPropsWithRef<'a'>)
  | ({ $url?: never } & ComponentPropsWithRef<'button'>)
) & {
  $url?: string;
  $spa?: boolean;
  'data-id'?: string;
  Root?: string | FC<any>;
};

export type ButtonRef = HTMLButtonElement | HTMLAnchorElement;

export const Button = forwardRef<ButtonRef, ButtonProps>(function (props, ref) {
  const isHash = props.$url?.startsWith('#');
  const {
    $spa = true,
    $url,
    Root = $url
      ? $spa && !isHash
        ? (props) => <NavLink {...props} />
        : 'a'
      : 'button',
    ...buttonProps
  } = props;
  delete buttonProps['data-id'];
  return (
    <Root
      data-button={props['data-id'] || ''}
      {...buttonProps}
      ref={ref}
      href={$url ? $url : undefined}
    />
  );
});
