import { ComponentPropsWithRef, forwardRef } from 'react';
import { getIdFromUrl } from 'utils/getIdFromUrl';
import { separateProps } from 'utils/separateProps';

export type ImgProps = ComponentPropsWithRef<'picture'> & {
  $src: string;
  $alt?: string;
};
export const Img = forwardRef<HTMLPictureElement, ImgProps>(function (
  props,
  ref,
) {
  const { $src, $alt, ...pictureProps } = props;
  const ext = $src.split('.').pop();
  const fileName = $src.split('/').pop()?.replace(`.${ext}`, '');
  const imgId = getIdFromUrl($src);
  return (
    <picture data-picture={imgId} {...pictureProps} ref={ref}>
      {ext !== 'svg' && <source srcSet={getSrcSet($src)} />}
      <img data-img={imgId} src={$src} alt={$alt || fileName} />
    </picture>
  );
});

export function getSrcSet(url: string) {
  const ext = url.split('.').pop();
  return [
    `${url.replace(`.${ext}`, '.')}${ext} 1x`,
    `${url.replace(`.${ext}`, '@2x.')}${ext} 2x`,
    `${url.replace(`.${ext}`, '@3x.')}${ext} 3x`,
  ].join(',');
}
