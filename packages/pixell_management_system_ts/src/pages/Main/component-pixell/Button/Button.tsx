import {
  ButtonRef,
  ButtonProps,
  Button,
} from 'pages/Main/component-common/Button';
import { forwardRef } from 'react';
import './Button.scss';
export const ButtonPx = forwardRef<ButtonRef, ButtonProps>(function (
  props,
  ref,
) {
  return <Button data-px {...props} ref={ref} />;
});
