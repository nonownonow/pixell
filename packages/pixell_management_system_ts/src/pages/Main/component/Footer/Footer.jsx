import { html } from 'utils/html';
import './Footer.scss';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { changeLanguage } from 'i18next';
import { ButtonPx } from 'pages/Main/component-pixell/Button/Button';
export function Footer() {
  const { t } = useTranslation('pixell');

  const [successMsg, setSusseccMsg] = useState('');
  const [errMsg, setErrorMsg] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState('');
  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const formObject = Object.fromEntries(formData.entries());
    try {
      setIsLoading(true);
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/landing/contact`,
        {
          method: 'POST',
          body: JSON.stringify(formObject),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      if (!response.ok) {
        setErrorMsg(t('footer.form.error'));
        setTimeout(() => {
          setErrorMsg('');
        }, 5000);
      }
      setSusseccMsg(t('footer.form.email.success'));
      setTimeout(() => {
        setSusseccMsg('');
      }, 5000);
    } catch (e) {
      setErrorMsg(t('footer.form.email.error'));
      setTimeout(() => {
        setErrorMsg('');
      }, 5000);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <footer data-footer>
      <aside data-sectioning="2" data-contact id="contact">
        <div data-content-wrapper="2">
          <h1 data-heading="2" {...html(t('footer.h'))} />
          <form onSubmit={onSubmitHandler}>
            <div data-input-wrapper>
              <input
                data-input
                data-email
                required
                id="email"
                name="email"
                type="email"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                placeholder={t('footer.form.email.placeholder')}
              />
              {successMsg && <p data-success-msg {...html(successMsg)} />}
              {errMsg && <p data-error-msg {...html(errMsg)} />}
            </div>
            <button
              data-button
              disabled={email === '' || isLoading}
              type="submit"
              {...html(t('footer.form.button_submit'))}
            />
          </form>
          <section data-sectioning="3" data-info>
            <h2 data-heading {...html(t('footer.table_title'))} />
            <ul data-table data-info>
              {t('footer.table', { returnObjects: true }).map((doc) => {
                const A = doc.email ? 'a' : 'div';
                return (
                  <li key={doc.id} data-item={doc.id}>
                    <A href={doc.email} target="_blank" rel="noreferrer">
                      <dl data-doc={doc.id}>
                        {['logo', 'label'].map((key) => (
                          <div data-entry={key} key={key}>
                            <dt data-key={key} {...html(key)} />
                            <dd
                              data-value
                              {...html(
                                format[key]
                                  ? format[key](doc[key], doc)
                                  : doc[key],
                              )}
                            />
                          </div>
                        ))}
                      </dl>
                    </A>
                  </li>
                );
              })}
            </ul>
          </section>
        </div>
      </aside>
    </footer>
  );
}
const format = {
  logo: (v, doc) => <img data-img src={v.src} alt={v.desc} />,
};
