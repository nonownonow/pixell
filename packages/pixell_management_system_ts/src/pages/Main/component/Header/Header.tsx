import { NavLink } from 'react-router-dom';
import { html } from 'utils/html';
import { ReactComponent as Menu } from './img/menu.svg';
import { ReactComponent as Close } from './img/close.svg';
import './Header.scss';
import { ComponentProps, ComponentPropsWithRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Logo } from './component/Logo';
import { ButtonPx } from 'pages/Main/component-pixell/Button/Button';
import { NavData } from 'type';

type HeaderProps = ComponentPropsWithRef<'header'>;

export function Header(props: HeaderProps) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { t } = useTranslation('pixell');
  return (
    <header data-header data-body {...props}>
      <div data-content-wrapper>
        <button data-menu-button onClick={() => setIsMenuOpen(true)}>
          <Menu />
        </button>
        <h1>
          <span data-sr-only {...html(t('h'))} />
          <NavLink to="/main">
            <Logo />
          </NavLink>
        </h1>
        <nav data-nav data-main-nav data-open={isMenuOpen}>
          <h2 data-sr-only data-heading="2" {...html(t('nav_h'))} />
          <button onClick={() => setIsMenuOpen(false)}>
            <Close data-button-close />
          </button>
          <ul>
            {(t('nav_data', { returnObjects: true }) as NavData[]).map(
              (doc) => (
                <li data-item key={doc.id}>
                  <a
                    href={doc.url}
                    {...html(doc.name)}
                    onClick={() => setIsMenuOpen(false)}
                  />
                </li>
              ),
            )}
          </ul>
          <NavLink
            to="/login"
            data-button
            data-primary
            data-login
            data-px
            data-mo
            {...html(t('button_login'))}
          />
        </nav>
        {/* todo: 모바일에서 hidden속성 넣기 */}
        <div data-button-group>
          <ButtonPx
            data-try
            data-primary
            $url="#contact"
            {...html(t('button_try'))}
          />
          <NavLink
            to="/login"
            data-button
            data-primary
            data-login
            data-pc
            data-px
            {...html(t('button_login'))}
          />
        </div>
      </div>
    </header>
  );
}
