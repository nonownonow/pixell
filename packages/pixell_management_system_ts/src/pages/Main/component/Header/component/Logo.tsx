import { useTranslation } from 'react-i18next';
import { ComponentPropsWithRef } from 'react';
import { Img } from 'pages/Main/component-common/Img';
import './Logo.scss';

export type LogoProps = ComponentPropsWithRef<'picture'>;
export function Logo(props: LogoProps) {
  const { t } = useTranslation('pixell');
  return (
    <Img data-pixell-logo $src={t('img_logo.src')} $alt={t('img_logo.alt')} />
  );
}
