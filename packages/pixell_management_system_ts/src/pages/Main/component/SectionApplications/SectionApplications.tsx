import { ReactComponent as Next } from './img/next-icon.svg';
import { html } from 'utils/html';
import './SectionApplications.scss';
import { useTranslation } from 'react-i18next';
import { Service } from 'type';
import { Img } from 'pages/Main/component-common/Img';
export function SectionApplications() {
  const { t } = useTranslation('main');
  return (
    <section data-sectioning="2" data-application>
      <div data-content-wrapper="2">
        <h2
          data-heading="2"
          {...html(t('sec_application.h'))}
          id="applications"
        />
        <section data-sectioning="3" data-sample>
          <h3 data-heading="3" {...html(t('sec_application.sec_services.h'))} />
          <ol data-list data-samples>
            {(
              t('sec_application.sec_services.list_sample_data', {
                returnObjects: true,
              }) as Service[]
            ).map((a, i) => (
              <li data-item key={i}>
                <div data-title>
                  <span data-numbering>0{i + 1}</span>
                  <span {...html(a.service)} />
                </div>

                <div data-image-compare>
                  <div data-image-card>
                    <Img data-img $src={a.before.img} $alt={a.before.desc} />
                    {a.before.info && <p data-info {...html(a.before.info)} />}
                  </div>
                  <div data-image-card>
                    <Img data-img $src={a.after.img} $alt={a.after.desc} />
                    {a.after.info && <p data-info {...html(a.after.info)} />}
                  </div>
                </div>
              </li>
            ))}
          </ol>
        </section>
        <section data-sectioning="3" data-production>
          <div data-section-root>
            <h3
              data-heading="3"
              {...html(t('sec_application.sec_production.h'))}
            />
            <p {...html(t('sec_application.sec_production.p'))} />
          </div>
          <Img
            data-img="production-img"
            $src="/pixell/img/production-img.webp"
            $alt={t('sec_application.sec_production.img_production')}
          />
          <div data-image-step>
            <Img
              data-img="img_before"
              $src="/pixell/img/img_before.webp"
              $alt={t('sec_application.sec_production.img_before')}
            />
            <Next data-icon="next" />
            <Img
              data-img="img_before"
              $src="/pixell/img/img_after.webp"
              $alt={t('sec_application.sec_production.img_after')}
            />
            <Next data-icon="next" />
            <Img
              $src="/pixell/img/img_res.webp"
              $alt={t('sec_application.sec_production.img_res')}
              data-pc
            />
            <img
              data-picture="result"
              data-mo
              src="/pixell/img/result.png"
              alt={t('sec_application.sec_production.img_res')}
            />
          </div>
        </section>
      </div>
    </section>
  );
}
