import { html } from 'utils/html';
import './SectionSolution.scss';

import { useTranslation } from 'react-i18next';
import { getSrcSet } from 'utils/getSrcSet';
import { Feature, ImgData } from 'type';
import { getIdFromUrl } from 'utils/getIdFromUrl';
import { Img } from 'pages/Main/component-common/Img';

export function SectionSolution() {
  const { t } = useTranslation('main');
  const a = t('sec_solution.sec_intro.list_award', {
    returnObjects: true,
  }) as any;
  return (
    <section data-sectioning="2" data-solution>
      <h2 data-heading="2" {...html(t('sec_solution.h'))} data-sr-only />
      <div data-content-wrapper="2">
        <section data-sectioning="3" data-intro id="solution">
          <h3 data-heading="3" {...html(t('sec_solution.sec_intro.h'))} />
          <Img
            $src={t('sec_solution.sec_intro.img.url')}
            $alt={t('sec_solution.sec_intro.img.alt')}
          />
          <div data-pc data-content-wrapper-p>
            <p {...html(t('sec_solution.sec_intro.p_1'))} />
            <p {...html(t('sec_solution.sec_intro.p_2'))} />
            <p {...html(t('sec_solution.sec_intro.p_3'))} />
          </div>
          <ul data-awards>
            {(
              t('sec_solution.sec_intro.list_award', {
                returnObjects: true,
              }) as ImgData[]
            ).map((a) => (
              <li key={a.id}>
                <Img data-img data-award-img $src={a.url} $alt={a.alt} />
                <p {...html(a.label)} />
              </li>
            ))}
          </ul>
        </section>
        <section data-sectioning="3" data-desc>
          <h3 data-heading="3" {...html(t('sec_solution.sec_desc.h'))} />
          <Img
            $src={t('sec_solution.sec_desc.img.url')}
            $alt={t('sec_solution.sec_desc.img.alt')}
          />
          <ul data-list data-features>
            {(
              t('sec_solution.sec_desc.list_feature_data', {
                returnObjects: true,
              }) as Feature[]
            ).map((a) => {
              if (a.url) {
                return (
                  <li data-item key={a.id}>
                    <Img
                      data-img={getIdFromUrl(a.url)}
                      $src={a.url}
                      $alt={a.desc}
                    />
                    <p data-title {...html(a.label)} />
                    <p data-desc {...html(a.desc)} />
                  </li>
                );
              }
              return (
                <li data-item key={a.id}>
                  <Img $src={a.url} $alt={a.alt} />
                  <p data-title {...html(a.label)} />
                  <p data-desc {...html(a.desc)} />
                </li>
              );
            })}
          </ul>
        </section>
      </div>
    </section>
  );
}
