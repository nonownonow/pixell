import { useTranslation } from 'react-i18next';
import './SectionWorkflow.scss';
import { html } from 'utils/html';
import { Img } from 'pages/Main/component-common/Img';
export function SectionWorkflow() {
  const { t } = useTranslation('main');
  return (
    <section data-sectioning="2" data-workflow>
      <div data-content-wrapper="2">
        <h2 data-heading="2" {...html(t('sec_workflow.h'))} id="workflow" />
        <section data-sectioning="3" data-desc>
          <h3 data-heading {...html(t('sec_workflow.sec_desc.h'))} />
          <Img
            data-img="workflow"
            $src="/pixell/img/workflow.webp"
            $alt={t('sec_workflow.sec_desc.p')}
          />
        </section>
        <section data-sectioning="3" data-provide>
          <h3 data-heading {...html(t('sec_workflow.sec_provide.h'))} />
          <ul data-list data-provide>
            {(
              t('sec_workflow.sec_provide.list_provide', {
                returnObjects: true,
              }) as Provide[]
            ).map((a) => (
              <li data-item key={a.id}>
                <Img data-img $src={a.img} $alt={`a ${a.provide} icon`} />
              </li>
            ))}
          </ul>
        </section>
      </div>
    </section>
  );
}

export type Provide = {
  id: string;
  img: string;
  provide: string;
};
