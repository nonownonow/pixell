import './styles/index.scss';
import './main.scss';

import { Header } from './component/Header/Header';
import { SectionSolution } from 'pages/Main/component/SectionSolution/SectionSolution';
import { Footer } from './component/Footer/Footer';
import { useTranslation } from 'react-i18next';
import { html } from 'utils/html';
import { Img } from './component-common/Img';
import { getImageSet } from 'utils/getImageSet';
import { SectionApplications } from './component/SectionApplications/SectionApplications';
import { SectionWorkflow } from './component/SectionWorkflow/SectionWorkflow';
import { changeLanguage } from 'i18next';
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';
import { Button } from './component-common/Button';
import { useCallback, useEffect } from 'react';
export function Main() {
  const { t } = useTranslation('main');
  const handlerScroll = useCallback(() => {
    const htmlStyle = window.getComputedStyle(document.documentElement);
    const rootFontSize = htmlStyle
      .getPropertyValue('font-size')
      .replace('px', '');
    const htmlFontSize = parseInt(rootFontSize);
    const offsetTop = 12 * htmlFontSize;
    const scroll = window.scrollY;
    const up = document.querySelector('[data-button="up"]');
    if (scroll > offsetTop) {
      up?.classList.add('show');
    } else {
      up?.classList.remove('show');
    }
  }, []);
  useEffect(() => {
    window.addEventListener('scroll', handlerScroll);
    return () => {
      window.removeEventListener('scroll', handlerScroll);
    };
  }, []);
  return (
    <div data-px="" id="px">
      <Header />
      <Button data-id="up" $url="#px">
        <ArrowCircleUpIcon />
      </Button>
      <main>
        <article data-sectioning="1" data-responsive data-main>
          <div data-sectioning="root">
            <Img $src={'/pixell/img/bg.webp'} />

            <div
              data-content-wrapper="1"
              style={
                {
                  '--pixell-bg': getImageSet('/pixell/img/pixell.webp'),
                } as any
              }
            >
              <Img $src={'/pixell/img/graphic.webp'} />
              <ul data-lang-list>
                <li>
                  <button onClick={() => changeLanguage('en')}>en</button>
                </li>
                <li>
                  <button onClick={() => changeLanguage('ko')}>ko</button>
                </li>
                <li>
                  <button onClick={() => changeLanguage('jp')}>jp</button>
                </li>
                <li>
                  <button onClick={() => changeLanguage('ch')}>ch</button>
                </li>
              </ul>
              <h2 data-heading="1" {...html(t('h'))} />
            </div>
          </div>

          <SectionSolution />
          <SectionApplications />
          <SectionWorkflow />
        </article>
      </main>
      <Footer />
    </div>
  );
}
