import { useRoutes } from 'react-router-dom';

// project import
import { lazy } from 'react';

// ==============================|| ROUTING RENDER ||============================== //
const Main = lazy(() =>
  import('pages/Main/main').then(({ Main }) => ({
    default: Main,
  })),
);

export default function ThemeRoutes() {
  return useRoutes([
    {
      path: '/',
      element: (
        <>
          <Main />
        </>
      ),
    },
    {
      path: '/login',
      element: (
        <>
          <div>LOGIN</div>
        </>
      ),
    },
  ]);
}
