export type ImgData = {
  id: string;
  url: string;
  alt: string;
  label: string;
};

export type Feature = ImgData & {
  desc: string;
};

export type Service = {
  service: string;
  before: {
    img: string;
    desc: string;
    info?: string;
  };
  after: {
    img: string;
    desc: string;
    info?: string;
  };
};

export type LogoData = {
  id: string;
  logo: {
    src: string;
    desc: string;
    link: string;
  };
  email: string;
};

export type NavData = {
  id: string;
  name: string;
  url: string;
};
