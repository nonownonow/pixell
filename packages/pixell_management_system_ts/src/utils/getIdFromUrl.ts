/* 파일 패스에서 파일 이름을 파일 아이디로 추출 */
export function getIdFromUrl(url: string) {
  const srcMatch = url.match(/.+\/(.+)[.].+$/);
  if (!srcMatch) {
    console.log(srcMatch);
    throw new Error('잘못된 파일 경로입니다.');
  }
  return srcMatch[1];
}
