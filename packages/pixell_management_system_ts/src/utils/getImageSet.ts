export function getImageSet(url: string) {
  const ext = url.split('.').pop();
  return (
    'image-set(' +
    [
      `url(${url.replace(`.${ext}`, '.')}${ext}) 1x`,
      `url(${url.replace(`.${ext}`, '@2x.')}${ext}) 2x`,
      `url(${url.replace(`.${ext}`, '@3x.')}${ext}) 3x`,
    ].join(',') +
    ')'
  );
}
