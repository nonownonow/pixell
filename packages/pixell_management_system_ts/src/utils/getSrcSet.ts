export function getSrcSet(option: Record<'x1' | 'x2' | 'x3', string>) {
  return [option.x1 + ' 1x', option.x2 + ' 2x', option.x3 + ' 3x'].join(',');
}
