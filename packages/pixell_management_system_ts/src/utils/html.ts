export function html(html?: string) {
  let result;
  if (typeof html === 'string') {
    // if (["string", "number", "boolean"].includes(typeof html)) {
    result = {
      dangerouslySetInnerHTML: {
        __html: html,
      },
    };
  } else {
    result = {
      children: html,
    };
  }
  return result;
}
