import { map, partition, pipe, some, toArray } from '@fxts/core';
import { CSSProperties } from 'react';
export function separateProps<P extends Record<string, any>>(
  props: P,
  rootPropsKeys: (string | RegExp)[] = [
    'className',
    'tabIndex',
    'style',
    'hidden',
    /data-.+/,
  ],
) {
  const rootPropsAndOtherProps = ([key]: string[]) =>
    pipe(
      rootPropsKeys,
      some((matcher) => RegExp(matcher).test(key)),
    );

  return pipe(
    Object.entries(props),
    partition(rootPropsAndOtherProps),
    map(Object.fromEntries),
    toArray,
  ) as [RootProps, RestProps<P>];
}

export interface RootProps {
  id?: string;
  className?: string;
  tabIndex?: number;
  style?: CSSProperties;
  [k: `data-${string}`]: string;
}
export type RestProps<P> = Omit<P, keyof RootProps>;
